// -- Ajax --
    // $.ajax({
    //     url: 'http://www.omdbapi.com/?apikey=a6023d33&s=avengers',
    //     success: movies => console.log(movies)
    // })
// --/. Ajax --

// -- Vanilla JS --
    // const xhr = new XMLHttpRequest()
    // xhr.onreadystatechange = function () {
    //     if(xhr.status === 200) {
    //         if(xhr.readyState === 4) {
    //             console.log(JSON.parse(xhr.response))
    //         }
    //     } else {
    //         console.log(xhr.responseText)
    //     }
    // }
    // xhr.open('get', 'http://www.omdbapi.com/?apikey=a6023d33&s=avengers')
    // xhr.send()
// -- /.Vanilla JS --

// -- Fetch --
    // fetch('http://www.omdbapi.com/?apikey=a6023d33&s=avengers')
    //     .then(response => response.json())
    //     .then(response => console.log(response))
// -- /.Fetch --

// -- Promise --
    // Promise adalah sebuah objek
    // janji (terpenuhi/ ingkar)
    // states (fulfilled/ rejected/ pending)
    // callback(resolved/ rejected/ finally)
    // aksi(then/ catch)

    // -- Contoh 1 --

    // -- Promise All
    // -- Promise All
        // let ditepati = false
        // const janji1 = new Promise((resolve, reject) => {
        //     if(ditepati){
        //         resolve('Janji ditepati!')
        //     } else {
        //         reject('Ingkar janji..')
        //     }
        // })

        // janji1
        //     .then(response => console.log('OK! : ' + response))
        //     .catch(response => console.log('NOT OK!: ' + response))
    // -- /.Contoh 1 --

    // -- Contoh 2 --
        // let ditepati = false
        // const janji2 = new Promise((resolve, reject) => {
        //     if(ditepati){
        //         setTimeout( () => {
        //             resolve('Ditepati setelah beberapa waktu!')
        //         }, 2000)
        //     } else {
        //         setTimeout( () => {
        //             resolve('Tidak ditepati setelah beberapa waktu!')
        //         }, 2000)
        //     }
        // })

        // console.log('mulai')

        // // console.log(janji2.then(()=>console.log(janji2)))
        // janji2
        //     .finally(() => console.log('selesai menunggu!'))
        //     .then(response => console.log('OK! : ' + response))
        //     .catch(response => console.log('NOT OK!: ' + response))
            
        // console.log('selesai')
    // -- /.Contoh 2 --

    // -- Promise All --
        const film = new Promise( resolve => {
            setTimeout( () => {
                resolve([{
                    judul: 'Avangers',
                    sutradara: 'Sandhika Galih',
                    pemeran: 'Doddy, Erik'
                }])
            }, 1000)
        })

        const cuaca = new Promise(resolve => {
            setTimeout( () => {
                resolve([{
                    kota: 'Bandung',
                    temp: 26,
                    kondisi: 'Cerah Berawan'
                }])
            }, 500)
        })

        // film.then(response => console.log(response))
        // cuaca.then(response => console.log(response))
        Promise.all([film, cuaca])
            // .then(response => console.log(response))
            .then(response => {
                const [film, cuaca] = response
                console.log(film)
                console.log(cuaca)
            })
    // -- /.Promise All --
// -- /.Promise --