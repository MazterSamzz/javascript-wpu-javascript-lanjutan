// ========= Promise review =========
    // const coba = new Promise( resolve => {
    //     setTimeout( () => {
    //         resolve('selesai')
    //     }, 2000)
    // })
    
    // coba.then( () => console.log(coba))
// ========= /.Promise review =========



function cobaPromise() {
    return new Promise ( (resolve, reject) => {
        const waktu = 3000

        if(waktu < 5000) {
            setTimeout( () => {
                resolve('selesai')
            }, waktu)
        } else {
            reject('kelamaan!')
        }
    })
}

// ========= Promise =========
    // const coba = cobaPromise()

    // coba
    //     .then(() => console.log(coba))
    //     .catch(() => console.log(coba))
// ========= /.Promise =========

// ========= Async Await =========
    async function cobaAsync() {
        // try = then, catch = catch
        try{
            const coba = await cobaPromise()
            console.log(coba)
        } catch (err) {
            console.log(err)
        }
    }

    cobaAsync()
// ========= /.Async Await =========