// // Cara membnuat Object pada javascript
// // 1. Object Literal
// let mahasiswa1 ={
//     nama: 'Sandhika',
//     energi: 10,
//     makan: function(porsi){
//         this.energi = this.energi + porsi;
//         console.log(`Halo ${this.nama}, selamat makan`);
//     }
// }

// let mahasiswa2 ={
//     nama: 'Doddy',
//     energi: 10,
//     makan: function(porsi){
//         this.energi = this.energi + porsi;
//         console.log(`Halo ${this.nama}, selamat makan`);
//     }
// }
// ========================================================================//
// // 2. Function Declaation
//     // Deklarasi
// function Mahasiswa(nama, energi){
//     let mahasiswa ={};
//     mahasiswa.nama = nama;
//     mahasiswa.energi = energi;

//     mahasiswa.makan = function(jml){
//         this.energi += jml;
//         console.log(`${this.nama}, makan ${jml} porsi!`);
//     }

//     mahasiswa.main =function(jml){
//         this.energi -= jml;
//         console.log(`${this.nama}, bermain ${jml} jam!`);
//     }

//     return mahasiswa;
// }

// let sandhika = Mahasiswa('Sandhika', 10);
// let doddy = Mahasiswa('Doddy', 10);
// ========================================================================//
//3. Constructor Function
//  //    Deklarasi
    function Mahasiswa(nama, energi){
        this.nama = nama;
        this.energi = energi;

        this.makan = function(jml){
            this.energi += jml;
            console.log(`${this.nama}, makan ${jml} porsi!`);
        }

        this.main =function(jml){
            this.energi -= jml;
            console.log(`${this.nama}, bermain ${jml} jam!`);
        }
    }

    let sandhika = new Mahasiswa('Sandhika', 10);
    let doddy = new Mahasiswa('Doddy', 10);
// ========================================================================//
// 4. Object.create