// // Cara membnuat Object pada javascript
// ========================================================================//
    // // 1. Object Literal
    // // PROBLEM: Tidak cocok untuk project yang banyak
    // // Deklarasi
    // let mahasiswa1 ={
    //     nama: 'Sandhika',
    //     energi: 10,
    //     makan: function(porsi){
    //         this.energi = this.energi + porsi;
    //         console.log(`Halo ${this.nama}, selamat makan`);
    //     }
    // }

    // let mahasiswa2 ={
    //     nama: 'Doddy',
    //     energi: 10,
    //     makan: function(porsi){
    //         this.energi = this.energi + porsi;
    //         console.log(`Halo ${this.nama}, selamat makan`);
    //     }
    // }
// ========================================================================//
    // // 2. Function Declaation
    // // Deklarasi
    // function Mahasiswa(nama, energi){
    //     let mahasiswa ={};
    //     mahasiswa.nama = nama;
    //     mahasiswa.energi = energi;

    //     mahasiswa.makan = function(jml){
    //         this.energi += jml;
    //         console.log(`${this.nama}, makan ${jml} porsi!`);
    //     }

    //     mahasiswa.main =function(jml){
    //         this.energi -= jml;
    //         console.log(`${this.nama}, bermain ${jml} jam!`);
    //     }

    //     return mahasiswa;
    // }

    // let sandhika = Mahasiswa('Sandhika', 10);
    // let doddy = Mahasiswa('Doddy', 10);
// ========================================================================//
    //3. Constructor Function
    // // Deklarasi
    // function Mahasiswa(nama, energi){
    //     this.nama = nama;
    //     this.energi = energi;

    //     this.makan = function(jml){
    //         this.energi += jml;
    //         console.log(`${this.nama}, makan ${jml} porsi!`);
    //     }

    //     this.main =function(jml){
    //         this.energi -= jml;
    //         console.log(`${this.nama}, bermain ${jml} jam!`);
    //     }
    // }

    // let sandhika = new Mahasiswa('Sandhika', 10);
    // let doddy = new Mahasiswa('Doddy', 10);
// ========================================================================//
// // 4. Object.create
// 
    // // Deklarasi ObjectMethod
    // Mahasiswa.prototype.makan: function(jml){
    //     this.energi += jml;
    //     console.log(`${this.nama}, makan ${jml} porsi!`);
    // },
    // Mahasiswa.prototype.main: function(jml){
    //     this.energi -= jml;
    //     console.log(`${this.nama}, bermain ${jml} jam!`);
    // },
    // Mahasiswa.prototype.tidur: function(jml){
    //     this.energi += jml*2;
    //     console.log(`${this.nama}, tidur ${jml} jam!`);
    // }
    // // Deklarasi Object
    // function Mahasiswa(nama, energi){
    //     this.nama = nama;
    //     this.energi = energi;
    // }

    // let sandhika = new Mahasiswa('Sandhika', 10);
    // let doddy = new Mahasiswa('Doddy', 10);

// =================== versi class ====================================
    class Mahasiswa{
        constructor(nama, energi){
            this.nama = nama;
            this.energi = energi;
        }

        makan(jml){
            this.energi += jml;
            console.log(`${this.nama}, makan ${jml} porsi!`);
        }
        main(jml){
            this.energi -= jml;
            console.log(`${this.nama}, bermain ${jml} jam!`);
        }
        tidur(jml){
            this.energi += jml*2;
            console.log(`${this.nama}, tidur ${jml} jam!`);
        }
    }
    let sandhika = new Mahasiswa('Sandhika', 10);
    let doddy = new Mahasiswa('Doddy', 10);