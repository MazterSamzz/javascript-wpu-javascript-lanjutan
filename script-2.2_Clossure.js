// function init(){
//     return function (nama){
//         console.log(nama);
//     }
// }
// let panggilNama = init();
// panggilNama('Shandika');
// panggilNama('Galih');

// function ucapkanSalam(waktu){
//     return function(nama){
//         console.log(`Halo ${nama}, Selamat ${waktu}, semoga hari mu menyenangkan!`);
//     }
// }

// let selamatPagi = ucapkanSalam('Pagi');
// let selamatSiang = ucapkanSalam('Siang');
// let selamatMalam = ucapkanSalam('Malam');

// selamatPagi('Sandhika');
// selamatMalam('Sandhika');


let add = (function(){
    let counter =0;
    return function(){
        return ++counter;
    }
})();

console.log(add());
console.log(add());
console.log(add());