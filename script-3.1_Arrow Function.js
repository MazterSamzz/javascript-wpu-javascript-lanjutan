// //Function Expression
// const tampilNama = function(nama){
//     return `Halo, ${nama}`;
// }
// console.log(tampilNama('Sandhika'));

// const tampilNama = (nama) => { return `Halo, ${nama}`};
// console.log('Doddy Ferdiansyah');

// // implisit return
// console.log('Doddy Ferdiansyah');



// const tampilNama = (nama, waktu) =>{
//  return `Selamat ${waktu}, ${nama}`;   
// }
// console.log(tampilNama('Erik', 'Malam'));
// ================================================
 let mahasiswa = ['Sandhika Galih', 'Doddy Ferdiansyah', 'Erik'];

// // --- CARA 1
// let jumlahHuruf = mahasiswa.map(function (nama){
//     return nama.length;
// });
// console.log(jumlahHuruf);

// --- CARA 2
let jumlahHuruf = mahasiswa.map(nama=>({nama: nama, jmlHuruf: nama.length}));
console.table(jumlahHuruf);



// ================================================