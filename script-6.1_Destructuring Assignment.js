// -- Destructuring Variable / Assignment

    // --  Destructuring Array --\
        // -- Destructuring --
            // const perkenalan  = ['Halo', 'nama', 'saya', 'Sandhika']

            // const salam = perkenalan[0]
            // const satu = perkenalan[1]
            // const dua = perkenalan[2]
            // const tiga = perkenalan[3]

            // const [salam, satu, dua, nama] = perkenalan
            // const [salam, , , nama] = perkenalan // <=== Skipping Items

            // console.log(nama)\
        // -- /.Destructuring --

        // -- Swap Items --
            // let a = 1
            // let b = 2
            // console.log(a, b);
            // [a,b] = [b,a]
            // console.log(a, b);
        // -- /.Swap Items --

        // -- Return value pada function --
            // function coba(){
            //     return [1,2]
            // }
            // const [a,b] = coba();
            // console.log(a,b)
        // -- /.Return value pada function --

        // -- Rest Parameter --
            // const [a,...values] = [1,2,3,4,5]
            // console.log(a, values)
        // -- /.Rest Parameter --
    // --  /.Destructuring Array --

// -- /.Destructuring Variable / Assignment
    // -- Destructuring Literal --
        // const mhs = {
        //     nama: 'Sandhika Galih',
        //     umur: 33
        // }

        // const {nama,umur} = mhs
        // console.log(nama, umur)
    // -- /.Destructuring Literal --

    // -- Assigntment witout Object Declaration --
        // (
        //     {nama:n , umur:u } = {nama: 'Sandhika Galih', umur:33}
        // )
        // console.log(n,u)
    // -- /.Assigntment witout Object Declaration --

    // -- Default Value --
        // const mhs = {
        //     nama: 'Sandhika Galih',
        //     umur: 33,
        //     email: 'sandhikagalih@unpas.ac.id'
        // }

        // const {nama,umur, email = 'email@default.com'} = mhs
        // console.log(nama,umur,email)
    // -- /.Default Value --

    // -- Default value + assign new variable --
        // const mhs = {
        //     nama: 'Sandhika Galih',
        //     umur: 33,
        //     email: 'sandhikagalih@unpas.ac.id'
        // }

        // const {nama:n, umur:u , email:e = 'email@default.com'} = mhs
        // console.log(n, u, e)
    // -- /.Default value + assign new variable --

    // -- Rest Parameter --
        // const mhs = {
        //     nama: 'Sandhika Galih',
        //     umur: 33,
        //     email: 'sandhikagalih@unpas.ac.id'
        // }

        // const {nama:n, ...value} = mhs
        // console.log(n, value)
    // -- /.Rest Parameter --

    // Mengambil field pada object, setelah dikirim sebagai parameter untuk function
        const mhs = {
            id: 123,
            nama: 'Sandhika Galih',
            umur: 33,
            email: 'sandhikagalih@unpas.ac.id'
        }

        function getIdMhs(mhs){
            return mhs.id;
        }
        console.log(getIdMhs(mhs))
    //