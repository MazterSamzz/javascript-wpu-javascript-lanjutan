// -- for..of --
    const mhs = ['Sandhika', 'Doddy', 'Erik']
    
    // -- Array --

        // // For
            // for (let i = 0; i < mhs.length; i++) {
            //     console.log(mhs[i])
            
        // }

        // //ForEach
            // mhs.forEach(m => console.log(m))

        // // For..of
            // for(const m of mhs) {
            //     console.log(m)
            // }
    
    // -- /.Array --

    // -- String --
        // // For..of
            // const nama = 'Sandhika'

            // for(const n of nama) {
            //     console.log(n)
            // }
        
        // // ForEach (Error not prototype Array)
            // nama.forEach(n => console.log(n))

        // // ForEach Array
            // mhs.forEach((m, i) => {
            //     console.log(`${m} adalah mahasiswa ke-${i+1}`)
            // })

        // For..of Array
            // for(const [i, m] of mhs.entries()) {
            //     console.log(`${m} adalah mahasiswa ke-${i+1}`)
            // }
    // -- /.String --

    // -- Node List --
        // const liNama = document.querySelectorAll('.nama')
        
        // // ForEach
        //     liNama.forEach(n => console.log(n.textContent))

        // // For..of
        //     for(n of liNama) {
        //         console.log(n.innerHTML)
        //     }

        // // Arguments
        //     function jumlahkanAngka() {
        //         let jumlah = 0
        //         for (a of arguments) {
        //             jumlah += a
        //         }
        //         return jumlah;
        //     }
        //     console.log(jumlahkanAngka(1, 2, 3, 4, 5))
    // -- /.Node List --

// -- /.for..of --

// -- For..In --
    const mhs1 = {
        nama: 'Sandhika',
        umur: 33,
        email: 'sandhikagalih@unpas.ac.id'
    }

    // // For..Of Error mhs1 is not iterable
        // for (m of mhs1) {
        //     console.log(m)
        // }

    // // For..In 
        for (m in mhs1) {
            console.log(mhs1[m])
        }
// -- /.For..In --