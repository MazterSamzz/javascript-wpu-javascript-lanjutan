// Spread Operator
// Memecah iterables menjadi single element

// const mhs = ['Sandhika', 'Doddy', 'Erik']
// const dosen = ['Ade', 'Hendra', 'Wanda']

// // const orang = mhs.concat(dosen)
// const orang = [...mhs, 'Aji', ...dosen]

// -- mengcopy array --
    // const mhs1 = mhs     // Ini salah
    // const mhs1 = [...mhs] // Ini benar

    //     mhs1[0] = 'Fajar'

    // console.log(mhs)
// -- /.mengcopy array --

// const liMhs = document.querySelectorAll('li')

// const mhs = []
// for(let i = 0; i<liMhs.length; i++) {
//     mhs.push(liMhs[i].textContent)
// }

// console.log(mhs)

// const mhs = [...liMhs].map(m => m.textContent)
// console.log(mhs)

const nama = document.querySelector('.nama')
const huruf = [...nama.textContent].map(h => `<span>${h}</span>`).join('')
nama.innerHTML =huruf